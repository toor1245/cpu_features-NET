#ifndef CPU_FEATURES_DOTNET_CPUINFO_AARCH64_PORT_H
#define CPU_FEATURES_DOTNET_CPUINFO_AARCH64_PORT_H

#include "cpu_features_port_macros.h"
#include "cpuinfo_aarch64.h"

CPU_FEATURES_DOTNET_DLL_EXPORT Aarch64Info GetAarch64InfoPort(void);

#endif // CPU_FEATURES_DOTNET_CPUINFO_AARCH64_PORT_H
